<?php

namespace App\Controller;

use App\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CategorieRepository;
use Doctrine\ORM\EntityManager;

class CategorieController extends AbstractController
 {
   private $repository;
    private $em;
  public function __construct(CategorieRepository $repository,EntityManagerInterface $em)
  {
    $this->repository=$repository;
    $this->em=$em;
    
  }
 
    /**
     * @Route("/categorie", name="app_categorie")
     */
    public function index(): Response
    {
       /*  $Cat = new Categorie();
        $Cat->setNom("categorie")->setDescription("description");
        $em=$this->getDoctrine()->getManager();
        $em->persist($Cat);
        $em->flush();
  */
        /* $categorieR=$this->repository->find(1);
        var_dump($categorieR);
     */

    $categorieR=$this->repository->findAll();
    $categorieR[0]->setNom("modif 1er bien");
    $this->em->flush();


        return $this->render('categorie/index.html.twig', [
            'controller_name' => 'CategorieController',
        ]);
    }
}
